#!/usr/bin/env python
import sys
from PIL import Image

mychar='''@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,"^`'. '''
length=len(mychar)
print ("mychar length:%s" % length)

def transform1(image_file):
    codePic = ''
    for h in range(0,image_file.size[1]):
        for w in range(0,image_file.size[0]):
#            print (image_file.getpixel((w,h)))
            g,r,b = image_file.getpixel((w,h)) #获取指定位置的像素的GRB值
            gray = int(r*0.2126 + g*0.7152 + b*0.0722)
            codePic = codePic +mychar[int(((length-1)*gray)/256)]
        codePic = codePic + '\r\n'
    return codePic

if __name__ == '__main__':

    image_file= sys.argv[1]
    im = Image.open(image_file)
    resizeim=im.resize((int(im.size[0]*0.5),int(im.size[1]*0.5)))
    newIm = open('/root/photo/baozou.txt','w')
    newIm.write(transform1(resizeim))
    im.close()
